﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using NaviсHelper.Lib.Extensions;
using NaviсHelper.Lib.Models;

namespace NaviсHelper.Lib.Steps
{
    /// <inheritdoc cref="SecondStep" />
    /// <summary>
    ///     Обработчик, принятие и обработка данных
    /// </summary>
    internal class SecondStep : Step
    {
        public SecondStep(SerialPort port) : base(port)
        {
            CommandsDictionary = new Dictionary<string, Action>
            {
                { Statuses.ONE_DIN_LOADER, () => Port.WriteCommand(Commands.READY) }, //transfer OK-->r
                { Statuses.UPDATER_START, () => Port.WriteCommand(Commands.GO_UPDATE) }, //UPDATER-->g
                { Statuses.UPDATER_READY, () =>
                    {
                        Port.WriteCommand(Commands.RESTART);
                        CallOnStateChange();
                    }
                }, //UPDATE OK-->k
                { Statuses.FIRMWARE_DONE, CallOnStateChange } //NAVIC_PRO_M -->r
            };
        }
    }
}