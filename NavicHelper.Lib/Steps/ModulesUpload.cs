﻿//using System;
//using System.Collections.Generic;
//using System.IO.Ports;
//using NaviсHelper.Lib.Extensions;
//using NaviсHelper.Lib.Models;

//namespace NaviсHelper.Lib.Steps
//{
//    internal class ModulesUpload : Step
//    {
//        public event Action OnFlashingStart; //Flashing (прошивать)

//        public ModulesUpload(SerialPort port) : base(port)
//        {
//            CommandsDictionary = new Dictionary<string, Action>
//            {
//                { Statuses.ONE_DIN_LOADER, () => Port.WriteCommand(Commands.UPDATE) }, //ONE-DIN-LOADER>-->u
//                { Statuses.ARE_YOU_SURE, () => Port.WriteCommand(Commands.CONFIRM) }, //SURE?[y/n]-->y
//                { Statuses.TRANSFER_DONE, () => Port.WriteCommand(Commands.READY) }, //TRANSFER OK-->r
//                { Statuses.INVITE_TO_1K_X_MODEM, () =>
//                    {
//                        try
//                        {
//                            OnFlashingStart?.Invoke();
//                            CallOnStateChange();
//                        }
//                        catch (Exception exception)
//                        {
//                            CallOnErrorOccured(exception.Message);
//                        }
//                    }
//                }
//            };
//        }
//    }
//}