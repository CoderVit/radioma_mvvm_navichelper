﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using NaviсHelper.Lib.Extensions;
using NaviсHelper.Lib.Models;

namespace NaviсHelper.Lib.Steps.NavicProPlus
{
    public class NavicPlusHandler : Step
    {
        private bool m_isStarted;
        public event Action<string> OnFlashingStart; // Старт прошивки устройства

        public NavicPlusHandler(SerialPort port, string firmwarePath) : base(port)
        {
            var path = firmwarePath ?? throw new ArgumentNullException(nameof(firmwarePath));

            CommandsDictionary = new Dictionary<string, Action>
            {
                { Statuses.NPM_BOOT, () =>
                {
                    Port.WriteCommand(!m_isStarted ? Commands.UPDATE : Commands.READY);
                    m_isStarted = !m_isStarted;
                } }, //NPM-BOOT>-->'u' or 'r'
                { Statuses.ARE_YOU_SURE_PLUS, () => Port.WriteCommand(Commands.CONFIRM) }, //SURE?[y/n]-->'y'
                { Statuses.INVITE_TO_1K_X_MODEM, () =>
                    {
                        try
                        {
                            OnFlashingStart?.Invoke(path);
                        }
                        catch (Exception exception)
                        {
                            CallOnErrorOccured(exception.Message);
                        }
                    }
                },
                { Statuses.FIRMWARE_PLUS_DONE, CallOnStateChange }
            };
        }
    }
}