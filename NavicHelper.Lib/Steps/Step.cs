﻿using System;
using System.Collections.Generic;
using System.IO.Ports;

namespace NaviсHelper.Lib.Steps
{
    /// <summary>
    /// Базовый класс для перехватывания информации, что поступает из КОМ-порта.
    /// </summary>
    public class Step
    {
        protected readonly SerialPort Port;

        protected Dictionary<string, Action> CommandsDictionary;
        protected string ResponseBuffer;

        //public event Action<string> OnSendToDevice;
        public event Action OnStateChange;
        public event Action<string> OnErrorOccured;

        public Step(SerialPort port)
        {
            Port = port ?? throw new ArgumentNullException(nameof(port));
        }

        public virtual void Handle(string response)
        {
            ResponseBuffer += response;
            foreach (var action in CommandsDictionary)
            {
                if (!ResponseBuffer.Contains(action.Key)) continue;
                ResponseBuffer = string.Empty;
                action.Value();
            }
        }

        //protected void CallOnDeviceSend(string command) => OnSendToDevice?.Invoke(command);
        protected void CallOnStateChange() => OnStateChange?.Invoke();
        protected void CallOnErrorOccured(string error) => OnErrorOccured?.Invoke(error);
    }
}