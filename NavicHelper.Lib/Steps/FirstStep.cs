﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using NaviсHelper.Lib.Extensions;
using NaviсHelper.Lib.Models;

namespace NaviсHelper.Lib.Steps
{
    /// <summary>
    ///     Обработчик, ответственный за принятие и обработку данных
    /// </summary>
    internal class FirstStep : Step
    {
        public event Action<string> OnFlashingStart; //Flashing (прошивать)

        public FirstStep(SerialPort port, string firmwarePath) : base(port)
        {
            var path = firmwarePath ?? throw new ArgumentNullException(nameof(firmwarePath));

            CommandsDictionary = new Dictionary<string, Action>
            {
                { Statuses.ONE_DIN_LOADER, () => Port.WriteCommand(Commands.UPDATE) }, //ONE-DIN-LOADER>-->u
                { Statuses.ARE_YOU_SURE, () => Port.WriteCommand(Commands.CONFIRM) }, //SURE?[y/n]-->y
                { Statuses.TRANSFER_DONE, () => Port.WriteCommand(Commands.READY) }, //TRANSFER OK-->r
                { Statuses.INVITE_TO_1K_X_MODEM, () =>
                    {
                        try
                        {
                            OnFlashingStart?.Invoke(path);  //запуск прошивки
                            CallOnStateChange();
                        }
                        catch (Exception exception)
                        {
                            CallOnErrorOccured(exception.Message);
                        }
                    }
                }
            };
        }
    }
}