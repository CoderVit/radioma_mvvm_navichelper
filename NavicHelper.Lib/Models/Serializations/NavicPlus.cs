﻿namespace NaviсHelper.Lib.Models.Serializations
{
    /// <summary>
    /// Модель устройства профиля Navic Plus
    /// </summary>
    public class NavicPlus : Profile
    {
        public NavicPlus()
        {
            NavicType = NavicType.NAVIC_PRO_PLUS;
        }

        /// <summary>
        ///  Путь к единому файлу прошивки устройства
        /// </summary>
        public string FirmwarePath { get; set; }
    }
}