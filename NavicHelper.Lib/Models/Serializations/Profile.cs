﻿using System.Xml.Serialization;

namespace NaviсHelper.Lib.Models.Serializations
{
    /// <summary>
    /// Модель профиля (конфигурации оборудования)
    /// </summary>
    [XmlInclude(typeof(Navic)), XmlInclude(typeof(NavicPlus))]
    public class Profile
    {
        /// <summary>
        /// Тип устройства
        /// </summary>
        [XmlIgnore]
        public NavicType NavicType { get; protected set; }

        /// <summary>
        /// Выбранный КОМ-порт устройства
        /// </summary>
        public string ComPort { get; set; }
        
    }
}