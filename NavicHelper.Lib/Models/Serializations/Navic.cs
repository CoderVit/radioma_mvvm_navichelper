﻿namespace NaviсHelper.Lib.Models.Serializations
{
    /// <summary>
    /// Модель устройства профиля Navic
    /// </summary>
    public class Navic : Profile
    {
        public Navic()
        {
            NavicType = NavicType.NAVIC_PRO;
        }

        /// <summary>
        /// Путь к файлу прошивки модулей устройства
        /// </summary>
        public string ModulesFirmwarePath { get; set; }

        /// <summary>
        /// Путь к файлу прошивки материнской платы устройства
        /// </summary>
        public string MotherboardFirmwarePath { get; set; }
    }
}