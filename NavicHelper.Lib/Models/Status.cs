﻿namespace NaviсHelper.Lib.Models
{
    /// <summary>
    /// Состояние работы приложения.
    /// </summary>
    public enum Status
    {
        STATUS_OFF,         //Выключено
        STATUS_CONNECTED,   //Подключено (открыт КОМ-порт)
        STATUS_FLASHING,    //Начат процесс "прошивки"
        STATUS_DONE         //Процесс "прошивки" успешно завершён
    }
}