﻿namespace NaviсHelper.Lib.Models
{
    /// <summary>
    ///     Перечисление, характеризирующие этап выполнения этапов пути хода прошивки
    /// </summary>
    internal enum Stage
    {
        STAGE_FREE = 0,
        STAGE_START,
        STAGE_UPLOAD,
        STAGE_UPDATE,
        STAGE_APPLY
    }
}