﻿namespace NaviсHelper.Lib.Models
{
    internal static class Commands
    {
        public const string START_TO_UPDATE = "%(RD:\r\n";
        public const string UPDATE = "u";
        public const string CONFIRM = "y";
        public const string READY = "r";
        public const string GO_UPDATE = "g";
        public const string RESTART = "k";
    }
}