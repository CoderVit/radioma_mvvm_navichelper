﻿using Prism.Mvvm;

namespace NaviсHelper.Lib.Models
{
    public class FirmwareModel : BindableBase
    {
        /// <summary>
        /// Адрес файла прошивки
        /// </summary>
        private string m_path;

        /// <summary>
        /// Геттер/Сеттер пути файла прошивки
        /// </summary>
        public string Path
        {
            get => m_path;
            set => SetProperty(ref m_path, value);
        }
    }
}