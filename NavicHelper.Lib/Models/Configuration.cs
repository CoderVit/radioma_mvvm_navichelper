﻿using System;
using NaviсHelper.Lib.Models.Serializations;

namespace NaviсHelper.Lib.Models
{
    /// <summary>
    /// Модель сохраненной конфигурации
    /// </summary>
    public class Configuration: ICloneable, IEquatable<Configuration>
    {
        /// <summary>
        /// Выбранное устройство
        /// </summary>
        public NavicType NavicType { get; set; }

        /// <summary>
        /// Скорость КОМ-порта
        /// </summary>
        public int BaudRate { get; set; }

        /// <summary>
        /// Профили устройств
        /// </summary>
        public Profile[] Profiles { get; set; }

        public object Clone() => MemberwiseClone();

        public bool Equals(Configuration other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return NavicType == other.NavicType && BaudRate == other.BaudRate && Equals(Profiles, other.Profiles);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Configuration) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (int) NavicType;
                hashCode = (hashCode * 397) ^ BaudRate;
                hashCode = (hashCode * 397) ^ (Profiles != null ? Profiles.GetHashCode() : 0);
                return hashCode;
            }
        }
    }
}