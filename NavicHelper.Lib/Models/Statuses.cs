﻿namespace NaviсHelper.Lib.Models
{
    internal static class Statuses
    {
        public const string ONE_DIN_LOADER = "ONE-DIN-LOADER>";
        public const string NPM_BOOT = "NPM-BOOT>";
        public const string ARE_YOU_SURE = "SURE?[y/n]";
        public const string ARE_YOU_SURE_PLUS = "Sure?[y/n]";
        public const string INVITE_TO_1K_X_MODEM = "1K-XMODEM";
        public const string UPDATER_START = "UPDATER";
        public const string UPDATER_READY = "UPDATE OK";
        public const string TRANSFER_DONE = "TRANSFER OK";
        public const string FIRMWARE_DONE = "NavicPRO 1DIN firmware";
        public const string FIRMWARE_PLUS_DONE = "NAVIC_PRO_M";
    }
}