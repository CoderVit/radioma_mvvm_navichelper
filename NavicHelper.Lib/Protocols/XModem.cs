using System;
using System.IO;
using System.IO.Ports;
using System.Threading;
using Navi�Helper.Lib.XModem;

namespace Navi�Helper.Lib.Protocols
{
    ////////////////////////////////////
    // Created by Andreas W�gerbauer  //
    // http://net.trackday.cc         //
    //                                //
    // Thanks to:                     //
    // Dave Kubasiak                  //
    ////////////////////////////////////

    //Version 1.3

    //This class is tested to be 100% compatibly to Microsoft(r) Hyperterminal 5.1

    //X-modemTransmit: supports both CRC and checksum
    //X-ModemReceive:  only supports checksum 

    //Feel free to use/modify/copy/distribute this file
    public class XModem
    {
        private const byte SOH = 0x01; //header for 128byte-packets 
        private const byte STX = 0x02; //header for 1024byte-packets 
        private const byte EOT = 0x04; //end of transmission 
        private const byte ACK = 0x06; //acknowledge
        private const byte NAK = 0x15; //negative acknowledge
        private const byte CAN = 0x18; //cancel transfer
        private const byte CTRL_Z = 0x1A; //padding char to fill data blocks < buffer size
        private const byte C = 0x43; //start of a CRC request

        private const ushort MAX_RETRANSMISSIONS = 25;
        private readonly Crc16 m_crc16 = new Crc16();
        private readonly SerialPort m_port;

        /// <summary>
        /// ������ ������ �������� �������� � ����������
        /// </summary>
        private bool m_isCancel;

        public XModem(SerialPort port)
        {
            m_port = port ?? throw new ArgumentNullException(nameof(port));
        }

        public event EventHandler PacketReceived;
        public event EventHandler PacketSent;

        ///////////////////////////
        //receive byte Array via XModem using either Checksum or CRC error detection
        /// ///////////////////////
        public byte[] XModemReceive(bool useCrc)
        {
            //since we don't know how many bytes we receive it's the
            //best solution to use a MemoryStream
            var buf = new MemoryStream();

            var packageNumber = 1;
            var retry = 0;
            double kb = 0;

            var buffer = new byte[1030]; //1024 for XModem 1k + 3 head chars + 2 crc + nul

            // send and use CRC or don't use CRC
            WriteByte(useCrc ? C : NAK);

            while (retry < 16)
            {
                int readByte;
                try
                {
                    readByte = m_port.ReadByte();
                }
                catch
                {
                    readByte = 0x00;
                }

                if (readByte == SOH || readByte == STX)
                {
                    #region SOH/STX

                    retry = 0;
                    var bufferSize = 128; // default buffer size
                    if (readByte == STX) bufferSize = 1024; // buffer size for Xmodem1K

                    //fill packet with data-stream

                    buffer[0] = (byte) readByte;
                    for (var index = 0; index < bufferSize + (useCrc ? 1 : 0) + 3; index++)
                        buffer[index + 1] = (byte) m_port.ReadByte();

                    if (buffer[1] == (byte) ~buffer[2] && buffer[1] == packageNumber &&
                        Check(buffer, bufferSize, useCrc))
                    {
                        //add buffer to memory stream
                        buf.Write(buffer, 3, bufferSize);
                        WriteByte(ACK);

                        #region fire event & increment packet number

                        //128 or 1024 Byte per package
                        double d = bufferSize;
                        d = d / 1024;
                        kb = kb + d;

                        PacketReceived?.Invoke(this, null);

                        packageNumber++;
                        if (packageNumber >= 256)
                            packageNumber = 0;

                        #endregion
                    }
                    else
                    {
                        WriteByte(NAK);
                    }

                    #endregion
                }
                else if (readByte == EOT)
                {
                    //EOT
                    m_port.DiscardInBuffer();
                    WriteByte(ACK);
                    //convert from memory stream to byte[]
                    return buf.ToArray();
                }
                else if (readByte == CAN)
                {
                    //CAN
                    if (m_port.ReadByte() == CAN)
                    {
                        m_port.DiscardInBuffer();
                        WriteByte(ACK);
                        return null; //canceled by remote
                    }
                }
                else
                {
                    retry++;
                    m_port.DiscardInBuffer();
                    WriteByte(NAK);
                }
            } //while()

            //timeout
            WriteByte(CAN);
            WriteByte(CAN);
            WriteByte(CAN);
            m_port.DiscardInBuffer();

            return null;
        }

        private bool Check(byte[] buffer, int size, bool isCrc)
        {
            const byte INDEX = 3;
            if (isCrc) // use CRC checking
            {
                var crc = m_crc16.CRC16_ccitt(buffer, size);
                var tcrc = (ushort) ((buffer[size + INDEX] << 8) + buffer[size + INDEX + 1]);
                if (crc == tcrc)
                    return true;
            }
            else
            {
                // use Checksum checking
                byte cks = 0;
                for (var i = 0; i < size; ++i) cks += buffer[i + INDEX];
                if (cks == buffer[size + INDEX])
                    return true;
            }

            return false;
        }

        /// <summary>
        ///     �������� ����� ��������
        /// </summary>
        /// <param name="source">����� ����� ��������</param>
        /// <param name="use1K">������������ �������� 1K X-Modem</param>
        /// <returns>success length or status codes</returns>
        public int Transmit(byte[] source, bool use1K)
        {
            m_isCancel = false;
            for (var retryCount = 0; retryCount < 16; ++retryCount)
            {
                var readByte = m_port.ReadByte();

                if (readByte < 0) continue;

                if (readByte == 'C') return Transmit(source, use1K, 1);

                if (readByte == NAK) return Transmit(source, use1K, 0);

                if (readByte == CAN)
                {
                    #region cancled by remote

                    if ((readByte = m_port.ReadByte()) == CAN)
                    {
                        WriteByte(ACK);
                        m_port.DiscardInBuffer();
                        return -1;
                    }

                    #endregion
                }
            } //for - retry

            WriteByte(CAN);
            WriteByte(CAN);
            WriteByte(CAN);
            m_port.DiscardInBuffer();
            return -2; //no sync
        }

        /// <summary>
        /// ���������� �������� �������� ����� ��������
        /// </summary>
        public void Cancel()
        {
            m_isCancel = true;
        }

        /// <summary>
        ///     �������� ��������
        /// </summary>
        /// <param name="source">����� ��������</param>
        /// <param name="use1K">������������  �������� 1K X-Modem</param>
        /// <param name="cyclicCheck">A cyclic redundancy check (CRC)</param>
        /// <returns>���-�� ���������� ������ ��� ��� ������</returns>
        private int Transmit(byte[] source, bool use1K, int cyclicCheck)
        {
            var sentLength = 0;
            var sourceSize = source.Length;
            byte packageNumber = 1;

            var buffer = new byte[1030]; //1024 for XModem 1k + 3 head chars + 2 crc + nul
            while (true)
            {
                var bufferSize = use1K ? 1024 : 128;

                buffer[0] = use1K ? STX : SOH;
                buffer[1] = packageNumber;
                buffer[2] = (byte) ~packageNumber;

                var sendLeft = sourceSize - sentLength; //len = data already sent
                if (sendLeft > bufferSize) sendLeft = bufferSize;
                if (sendLeft > 0)
                {
                    //clear buffer
                    //sets the first num bytes pointed by buffer to the value specified by CTRLZ parameter.
                    const byte STEP = 3;
                    for (var index = 0; index < bufferSize; index++) buffer[index + STEP] = CTRL_Z;

                    //fill 'buffer' with data
                    //copies num bytes from src buffer to memory location pointed by dest.

                    for (var index = 0; index < sendLeft; index++) buffer[index + STEP] = source[index + sentLength];

                    //create CRC / Checksum
                    if (cyclicCheck == 1)
                    {
                        var ccrc = m_crc16.CRC16_ccitt(buffer, bufferSize);
                        buffer[bufferSize + 3] = (byte) ((ccrc >> 8) & 0xFF);
                        buffer[bufferSize + 4] = (byte) (ccrc & 0xFF);
                    }
                    else
                    {
                        byte ccks = 0;
                        for (var index = 3; index < bufferSize + 3; ++index) ccks += buffer[index];
                        buffer[bufferSize + 3] = ccks;
                    }

                    var retryCount = 0;
                    var success = false;

                    while (retryCount < MAX_RETRANSMISSIONS && !success)
                    {
                        retryCount++;
                        if (m_isCancel) throw new OperationCanceledException("�������� ��������");

                        //write 'buffer' out on port
                        m_port.Write(buffer, 0, bufferSize + 4 + cyclicCheck);

                        var readByte = m_port.ReadByte();

                        if (readByte < 0) continue;

                        if (readByte == ACK)
                        {
                            PacketSent?.Invoke(this, null);

                            ++packageNumber;
                            sentLength += bufferSize;
                            success = true; //go ahead in transmission
                        }

                        if (readByte == CAN)
                            if ((readByte = m_port.ReadByte()) == CAN)
                            {
                                WriteByte(ACK);
                                m_port.DiscardInBuffer();
                                return -1; //canceled by remote
                            }

                        if (readByte == NAK)
                        {
                            //do nothing
                        }
                    } //while

                    if (!success)
                    {
                        //xmit error
                        WriteByte(CAN);
                        WriteByte(CAN);
                        WriteByte(CAN);
                        m_port.DiscardInBuffer();
                        return -4;
                    }
                }
                else
                {
                    //transfer completed
                    var lastByte = 0;
                    for (var retryCount = 0; retryCount < 10; ++retryCount)
                    {
                        WriteByte(EOT);

                        //avoid problem with to short timeout on port
                        Thread.Sleep(500);

                        lastByte = m_port.ReadByte();
                        if (lastByte == ACK)
                            break;
                    }

                    //port.DiscardInBuffer();
                    return lastByte == ACK ? sentLength : -5;
                }
            } //while
        }

        /// <summary>
        ///     �������� ������ �����
        /// </summary>
        /// <param name="byteToSend">���� ��� ��������</param>
        private void WriteByte(byte byteToSend)
        {
            m_port.Write(new[] {byteToSend}, 0, 1);
        }
    }
}