﻿using System;
using System.IO;

namespace NaviсHelper.Lib.Protocols
{
    public class Client
    {
        private readonly XModem m_xModem;

        private const short SEND_STEP = 1024;

        private int m_bytesSent;

        private int m_firmwareLength;

        public Action<int> SendProgress;

        public Client(XModem xModem)
        {
            m_xModem = xModem ?? throw new ArgumentNullException(nameof(xModem));

            xModem.PacketSent += OnPackageSend;
        }

        public void Send(byte[] firmwareBytes)
        {
            m_firmwareLength = firmwareBytes.Length;

            if (firmwareBytes == null || m_firmwareLength  == 0) throw new NullReferenceException("Прошивка не была загружена");

            var uploadedSize = m_xModem.Transmit(firmwareBytes, true);

            if (uploadedSize < m_firmwareLength) throw new Exception("Ошибка передачи файла " + uploadedSize);
        }

        /// <summary>
        /// Сброс на исходные значения
        /// </summary>
        public void Reset()
        {
            m_bytesSent = m_firmwareLength = 0;
        }

        /// <summary>
        /// Прерывание процесса прошивки
        /// </summary>
        public void Cancel()
        {
            SendProgress?.Invoke(0);
            m_xModem.Cancel();
        }

        private void OnPackageSend(object sender, EventArgs args)
        {
            m_bytesSent += SEND_STEP;
            SendProgress?.Invoke(Math.Min(m_bytesSent, m_firmwareLength) * 100 / m_firmwareLength);
        }
    }
}