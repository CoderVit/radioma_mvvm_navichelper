﻿using System.Diagnostics;
using System.IO.Ports;
using System.Text;

namespace NaviсHelper.Lib.Extensions
{
    internal static class PortExtensions
    {
        public static void WriteCommand(this SerialPort port, string command)
        {
            var commandBytes = Encoding.ASCII.GetBytes(command);
            port.Write(commandBytes, 0, commandBytes.Length);
            Debug.WriteLine($"<< {command}");
        }
    }
}