﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace NaviсHelper.Lib.Extensions
{
    internal static class EnumExtensions
    {
        public static string GetDescription(this Enum enums) => enums
                                                                    .GetType()
                                                                    .GetMember(enums.ToString())
                                                                    .FirstOrDefault()
                                                                    ?.GetCustomAttribute<DescriptionAttribute>()
                                                                    ?.Description
                                                                ?? enums.ToString();
    }
}