﻿using System.IO.Ports;
using NaviсHelper.Lib.Protocols;
using NaviсHelper.Lib.Steps.NavicProPlus;

namespace NaviсHelper.Lib.Flashers
{
    public class NavicPlusFlasher : FlasherBase
    {
        public NavicPlusFlasher(SerialPort port, Client xModemClient) : base(port, xModemClient) { }

        public void InitHandler(NavicPlusHandler handler)
        {
            Handler = handler;
            Handler.OnFlashingStart += UploadFirmware;
            Handler.OnErrorOccured += OnErrorOccured;
            Handler.OnStateChange += RaiseOnProcessFinish;
        }
    }
}