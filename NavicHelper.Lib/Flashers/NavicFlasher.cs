﻿using System;
using System.Diagnostics;
using System.IO;
using System.IO.Ports;
using NaviсHelper.Lib.Extensions;
using NaviсHelper.Lib.Models;
using NaviсHelper.Lib.Protocols;
using NaviсHelper.Lib.Steps;

namespace NaviсHelper.Lib.Flashers
{
    public class NavicFlasher
    {
        private readonly SerialPort m_port;

        private readonly Client m_xModemClient;

        private string m_modulesFirmware;
        private string m_motherboardFirmware;

        private Stage m_stage;

        private Step m_step;

        public NavicFlasher(SerialPort port)
        {
            m_port = port ?? throw new ArgumentNullException(nameof(port));
            m_xModemClient = new Client(new Protocols.XModem(port)) { SendProgress = OnFlashingModules };
        }

        public event Action<string> OnDataReceived;
        public event Action<string> OnErrorOccured;
        public event Action<int> OnFlashingModules;
        public event Action<int> OnFlashingMotherBoard;
        public event Action OnProcessFinish;

        public void InitFirmware(string modulesFirmware, string motherboardFirmware)
        {
            m_modulesFirmware = modulesFirmware ?? throw new ArgumentNullException(nameof(modulesFirmware));
            m_motherboardFirmware = motherboardFirmware ?? throw new ArgumentNullException(nameof(motherboardFirmware));
        }

        /// <summary>
        /// Отправка команды для старта прошивки
        /// </summary>
        public void Start()
        {
            m_stage = Stage.STAGE_START;

            var step = new FirstStep(m_port, m_modulesFirmware);
            step.OnFlashingStart += UploadFirmware;
            step.OnErrorOccured += OnErrorOccured;
            step.OnStateChange += () =>
            {
                m_stage++; //to upload
                m_step = new SecondStep(m_port);
                m_step.OnStateChange += NextState;
            };

            m_step = step;

            m_xModemClient.SendProgress = OnFlashingModules;
            m_port.WriteCommand(Commands.START_TO_UPDATE);
        }

        public void Cancel()
        {
            m_xModemClient.Cancel();
        }

        /// <summary>
        /// Этап после неудачной закачки прошивки
        /// </summary>
        public void RunFromUpdater(string motherboardFirmware)
        {
            m_motherboardFirmware = motherboardFirmware ?? throw new ArgumentNullException(nameof(motherboardFirmware));
            m_stage = Stage.STAGE_START;
            m_port.WriteCommand(Commands.GO_UPDATE);
            NextState();
        }

        public void Handle(string response)
        {
            m_step?.Handle(response);
        }

        /// <summary>
        /// Этап если пишет "BOOT" постоянно
        /// </summary>
        public void RunFromBoot(string motherboardFirmware)
        {
            m_motherboardFirmware = motherboardFirmware ?? throw new ArgumentNullException(nameof(motherboardFirmware));

            m_stage = Stage.STAGE_UPLOAD;
            m_port.WriteCommand(Commands.UPDATE);
            NextState();
        }

        private void NextState()
        {
            switch (m_stage)
            {
                case Stage.STAGE_UPLOAD: //статус запуска обновления ПО в контроллере
                    m_stage++; //to update
                    var step2 = new FirstStep(m_port, m_motherboardFirmware);
                    step2.OnFlashingStart += UploadFirmware;
                    step2.OnErrorOccured += OnErrorOccured;
                    step2.OnStateChange += () =>
                    {
                        m_stage++; //to apply
                        m_step = new SecondStep(m_port);
                        m_step.OnStateChange += () =>
                        {
                            OnProcessFinish?.Invoke();
                            m_step = null;
                        };
                    };
                    m_step = step2;
                    m_xModemClient.SendProgress = OnFlashingMotherBoard; //переназначаем процент передачи прошивки в контроллер на второй обработчик
                    break;
                default:
                    OnErrorOccured?.Invoke("Срабатывание ошибочного статуса");
                    break;
            }

            Debug.WriteLine("NEXT STATE --> {0}", m_stage);
        }

        /// <summary>
        /// Загрузка ПО в контроллер по протоколу XModem-1K
        /// </summary>
        private void UploadFirmware(string path)
        {
            OnDataReceived?.Invoke($"{Environment.NewLine}Загрузка файла {Path.GetFileName(path)}{Environment.NewLine}");

            if (File.Exists(path))
            {
                try
                {
                    var bytes = File.ReadAllBytes(path);
                    m_xModemClient.Send(bytes);
                }
                catch (OperationCanceledException)
                {
                    m_xModemClient.Cancel();
                    OnErrorOccured?.Invoke("Операция отменена");
                    throw;
                }
                catch (Exception exception)
                {
                    OnErrorOccured?.Invoke(exception.Message);
                    throw;
                }
                finally
                {
                    m_xModemClient.Reset();
                }
            }
            else
            {
                OnErrorOccured?.Invoke("Файл прошивки не найден");
            }
        }
    }
}