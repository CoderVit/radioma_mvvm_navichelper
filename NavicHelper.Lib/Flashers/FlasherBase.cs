﻿using System;
using System.IO;
using System.IO.Ports;
using NaviсHelper.Lib.Extensions;
using NaviсHelper.Lib.Models;
using NaviсHelper.Lib.Protocols;
using NaviсHelper.Lib.Steps.NavicProPlus;

namespace NaviсHelper.Lib.Flashers
{
    public class FlasherBase
    {
        protected readonly SerialPort Port;
        private readonly Client m_xModemClient;

        protected NavicPlusHandler Handler;

        #region События

        /// <summary>
        /// Событие вызывающиеся при получении данных с КОМ-порта.
        /// </summary>
        public event Action<string> DataReceived;

        /// <summary>
        /// Событие статуса процесса устройства
        /// </summary>
        public event Action<int> FlashingModules;

        /// <summary>
        /// Событие вызывающиеся при завершении процесса прошивки.
        /// </summary>
        public event Action OnProcessFinish;

        /// <summary>
        /// Событие вызывающиеся при возникновении ошибки.
        /// </summary>
        public event Action<string> ErrorOccured;

        #endregion События

        public void OnFlashingModules(int percent) => FlashingModules?.Invoke(percent);
        protected void RaiseOnProcessFinish() => OnProcessFinish?.Invoke();
        public void OnErrorOccured(string error) { ErrorOccured?.Invoke(error); }

        public FlasherBase(SerialPort port, Client xModemClient)
        {
            Port = port ?? throw new ArgumentNullException(nameof(port));
            m_xModemClient = xModemClient ?? throw new ArgumentNullException(nameof(xModemClient));
        }

        /// <summary>
        /// Запуск процесса прошивки
        /// </summary>
        public virtual void Start()
        {
            Port.WriteCommand(Commands.START_TO_UPDATE);
        }

        /// <summary>
        /// Сброс решение
        /// </summary>
        public virtual void Cancel()
        {
            m_xModemClient.Cancel();
        }

        /// <summary>
        /// Загрузка ПО в контроллер по протоколу XModem-1K
        /// </summary>
        protected void UploadFirmware(string path)
        {
            DataReceived?.Invoke($"{Environment.NewLine}Загрузка файла {Path.GetFileName(path)}{Environment.NewLine}");

            if (File.Exists(path))
            {
                try
                {
                    var bytes = File.ReadAllBytes(path);
                    m_xModemClient.Send(bytes);
                }
                catch (OperationCanceledException)
                {
                    m_xModemClient.Cancel();
                    ErrorOccured?.Invoke("Операция отменена");
                    throw;
                }
                catch (Exception exception)
                {
                    ErrorOccured?.Invoke(exception.Message);
                    throw;
                }
                finally
                {
                    m_xModemClient.Reset();
                }
            }
            else
            {
                ErrorOccured?.Invoke("Файл прошивки не найден");
            }
        }

        public void Handle(string response) => Handler?.Handle(response);
    }
}