﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using NaviсHelper.Lib.Models;
using Prism.Mvvm;

namespace NaviсHelper.Lib.ViewModels
{
    public class PortViewModel : BindableBase
    {
        /// <summary>
        /// Скорость КОМ-порта
        /// </summary>
        public int BaudRate { get; set; }

        /// <summary>
        /// Текущий статус приложения
        /// </summary>
        private Status m_status;
        /// <summary>
        /// Геттер/Сеттер m_status
        /// </summary>
        public Status Status
        {
            get => m_status;
            set => SetProperty(ref m_status, value);
        }

        private ObservableCollection<string> m_ports;
        public ReadOnlyObservableCollection<string> ComPorts;

        public void Validate(List<string> errorsList)
        {
            if (BaudRate == 0)
            {
                errorsList.Add("Не задана скорость КОМ-порта");
            }
        }

        /// <summary>
        /// Обновление списка ком-портов системы
        /// </summary>
        public void UpdateComPorts(string[] comPorts)
        {
            m_ports = new ObservableCollection<string>(comPorts);
            ComPorts = new ReadOnlyObservableCollection<string>(m_ports);
            RaisePropertyChanged(nameof(ComPorts));
        }
    }
}