﻿using System.Collections.ObjectModel;
using System.Linq;
using NaviсHelper.Lib.ViewModels.Profile;
using Prism.Mvvm;

namespace NaviсHelper.Lib.ViewModels
{
    public class ProfilesViewModel : BindableBase
    {
        private ProfileViewModel m_profileModel;
        public ProfileViewModel SelectedProfile
        {
            get => m_profileModel;
            set => SetProperty(ref m_profileModel, value);
        }

        private readonly ObservableCollection<ProfileViewModel> m_profiles;
        public readonly ReadOnlyObservableCollection<ProfileViewModel> Profiles;

        public ProfilesViewModel()
        {
            m_profiles = new ObservableCollection<ProfileViewModel>();
            Profiles = new ReadOnlyObservableCollection<ProfileViewModel>(m_profiles);
        }

        public void AddProfile(ProfileViewModel model)
        {
            if (m_profiles.All(profileModel => profileModel.NavicType != model.NavicType))
            {
                m_profiles.Add(model);
            }
        }
    }
}