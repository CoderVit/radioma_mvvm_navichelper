﻿using System.Collections.Generic;
using NaviсHelper.Lib.Models;
using Prism.Mvvm;

namespace NaviсHelper.Lib.ViewModels.Profile
{
    public abstract class ProfileViewModel : BindableBase
    {
        #region Properties

        public NavicType NavicType { get; protected set; }

        /// <summary>
        /// Выбранный КОМ-порт
        /// </summary>
        private string m_comPort;

        /// <summary>
        /// Геттер/Сеттер m_selectedPort
        /// </summary>
        public string ComPort
        {
            get => m_comPort;
            set => SetProperty(ref m_comPort, value);
        }

        private bool m_isConfigChange;
        public bool IsConfigChange
        {
            get => m_isConfigChange;
            set => SetProperty(ref m_isConfigChange, value);
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Проверка профиля
        /// </summary>
        /// <param name="errorsList">Список ошибок</param>
        public abstract void Validate(List<string> errorsList);
        //{
        //    if (string.IsNullOrEmpty(m_comPort))
        //    {
        //        errorsList.Add("Не задан КОМ-порт");
        //    }
        //}

        public abstract void ResetProgress();

        #endregion Methods
    }
}