﻿using System.Collections.Generic;
using System.IO;
using NaviсHelper.Lib.Models;

namespace NaviсHelper.Lib.ViewModels.Profile
{
    public class NavicViewModel : ProfileViewModel
    {
        public NavicViewModel()
        {
            NavicType = NavicType.NAVIC_PRO;
        }

        #region Properties

        /// <summary>
        /// Процент загрузки прошивки-модулей в устройство
        /// </summary>
        private int m_modulesFlashingProcess;

        /// <summary>
        /// Геттер/сеттер m_modulesFlashingProcess
        /// </summary>
        public int ModulesFlashingProcess
        {
            get => m_modulesFlashingProcess;
            set => SetProperty(ref m_modulesFlashingProcess, value);
        }

        /// <summary>
        /// Модель прошивки-модулей устройства
        /// </summary>
        public FirmwareModel ModulesFirmware { get; } = new FirmwareModel();

        /// <summary>
        /// Процент загрузки прошивки материнской платы в устройство
        /// </summary>
        private int m_motherboardFlashingProcess;

        /// <summary>
        /// Геттер/сеттер m_motherboardFlashingProcess
        /// </summary>
        public int MotherboardFlashingProcess
        {
            get => m_motherboardFlashingProcess;
            set => SetProperty(ref m_motherboardFlashingProcess, value);
        }

        /// <summary>
        /// Модель прошивки материнской платы
        /// </summary>
        public FirmwareModel MotherboardFirmware { get; } = new FirmwareModel();

        #endregion Properties

        #region Methods

        public override void Validate(List<string> errorsList)
        {
            if (!File.Exists(ModulesFirmware.Path) || !File.Exists(MotherboardFirmware.Path))
            {
                errorsList.Add("Файлы прошивок не найдены");
            }
        }

        public override void ResetProgress()
        {
            ModulesFlashingProcess = MotherboardFlashingProcess = 0;
        }

        #endregion Methods
    }
}