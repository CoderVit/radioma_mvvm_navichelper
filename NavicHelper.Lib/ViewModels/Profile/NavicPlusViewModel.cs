﻿using System.Collections.Generic;
using System.IO;
using NaviсHelper.Lib.Models;

namespace NaviсHelper.Lib.ViewModels.Profile
{
    public class NavicPlusViewModel : ProfileViewModel
    {
        public NavicPlusViewModel()
        {
            NavicType = NavicType.NAVIC_PRO_PLUS;
        }

        #region Properties

        public FirmwareModel Firmware { get; set; } = new FirmwareModel();

        private int m_firmwareProgress;
        public int FirmwareProgress
        {
            get => m_firmwareProgress;
            set => SetProperty(ref m_firmwareProgress, value);
        }

        #endregion Properties

        #region Methods

        public override void Validate(List<string> errorsList)
        {
            if (!File.Exists(Firmware.Path))
            {
                errorsList.Add("Отсутствует файл прошивки");
            }

            //base.Validate(errorsList);
        }

        public override void ResetProgress()
        {
            FirmwareProgress = 0;
        }

        #endregion Methods
    }
}