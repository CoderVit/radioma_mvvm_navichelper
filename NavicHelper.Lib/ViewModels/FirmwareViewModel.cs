﻿//using System.Collections.Generic;
//using System.IO;
//using Prism.Mvvm;

//namespace NavicHelper.UI.ViewModels
//{
//    internal class FirmwareViewModel : BindableBase
//    {
//        public FirmwareViewModel()
//        {
//            ModulesFirmware.PropertyChanged += (sender, args) => RaisePropertyChanged(nameof(ModulesFirmware));
//            MotherboardFirmware.PropertyChanged += (sender, args) => RaisePropertyChanged(nameof(MotherboardFirmware));
//            Firmware.PropertyChanged += (sender, args) => RaisePropertyChanged(nameof(Firmware));
//        }

//        public FirmwareModel ModulesFirmware { get; } = new FirmwareModel();

//        public FirmwareModel MotherboardFirmware { get; } = new FirmwareModel();

//        public FirmwareModel Firmware { get; } = new FirmwareModel();

//        public void Validate(List<string> errors)
//        {
//            if (File.Exists(ModulesFirmware.Path) && File.Exists(MotherboardFirmware.Path)) return;
//            errors.Add("Не выбраны файлы прошивок");
//        }

//        public void ValidatePlus(List<string> errors)
//        {
//            if (File.Exists(Firmware.Path)) return;
//            errors.Add("Не выбран файл прошивки");
//        }
//    }
//}