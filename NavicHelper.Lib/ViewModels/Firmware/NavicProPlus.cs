﻿using System.Collections.Generic;
using System.IO;
using NaviсHelper.Lib.Models;

namespace NavicHelper.UI.ViewModels.Firmware
{
    public class NavicProPlus : NaviсHelper.Lib.ViewModels.Firmware.Firmware
    {
        private FirmwareModel m_deviceFirmware = new FirmwareModel();
        public FirmwareModel DeviceFirmware
        {
            get => m_deviceFirmware;
            set => SetProperty(ref m_deviceFirmware, value);
        }
        public override void Validate(List<string> errors)
        {
            if (File.Exists(m_deviceFirmware.Path)) return;
            errors.Add("Не выбран файл прошивки");
        }
    }
}