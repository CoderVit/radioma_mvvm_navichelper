﻿using System.Collections.Generic;
using System.IO;
using NaviсHelper.Lib.Models;

namespace NaviсHelper.Lib.ViewModels.Firmware
{
    public class NavicPro : Firmware
    {
        private FirmwareModel m_modulesFirmware = new FirmwareModel();
        public FirmwareModel DeviceModulesFirmware
        {
            get => m_modulesFirmware;
            set => SetProperty(ref m_modulesFirmware, value);
        }

        private FirmwareModel m_motherboardFirmware = new FirmwareModel();
        public FirmwareModel DeviceMotherboardFirmware
        {
            get => m_motherboardFirmware;
            set => SetProperty(ref m_motherboardFirmware, value);
        }

        public override void Validate(List<string> errors)
        {
            if (File.Exists(m_modulesFirmware.Path) && File.Exists(m_motherboardFirmware.Path)) return;
            errors.Add("Не выбраны файлы прошивок");
        }
    }
}