﻿using System.Collections.Generic;
using Prism.Mvvm;

namespace NaviсHelper.Lib.ViewModels.Firmware
{
    public abstract class Firmware: BindableBase
    {
        public abstract void Validate(List<string> errors);
    }
}