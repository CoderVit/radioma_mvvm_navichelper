﻿using NaviсHelper.Lib.Models;
using Prism.Mvvm;

namespace NaviсHelper.Lib.ViewModels
{
    public class DeviceTypeViewModel : BindableBase
    {
        private NavicType m_navicType;
        public NavicType NavicType 
        {
            get => m_navicType;
            set => SetProperty(ref m_navicType, value);
        }
    }
}