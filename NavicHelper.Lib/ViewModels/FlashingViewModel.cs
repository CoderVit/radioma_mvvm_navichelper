﻿using Prism.Mvvm;

namespace NaviсHelper.Lib.ViewModels
{
    public class FlashingViewModel : BindableBase
    {
        private int m_modulesFlashingProcess;
        public int ModulesFlashingProcess
        {
            get => m_modulesFlashingProcess;
            set => SetProperty(ref m_modulesFlashingProcess, value);
        }

        private int m_motherboardFlashingProcess;
        public int MotherboardFlashingProcess
        {
            get => m_motherboardFlashingProcess;
            set => SetProperty(ref m_motherboardFlashingProcess, value);
        }

        private int m_globalFlashingProcess;
        public int GlobalFlashingProcess
        {
            get => m_globalFlashingProcess;
            set => SetProperty(ref m_globalFlashingProcess, value);
        }

        private string m_comPortData;
        public string ComPortData
        {
            get => m_comPortData;
            set => SetProperty(ref m_comPortData, value);
        }

        #region Methods

        public void ResetModel() => ModulesFlashingProcess = MotherboardFlashingProcess = GlobalFlashingProcess = 0;

        #endregion Methods
    }
}