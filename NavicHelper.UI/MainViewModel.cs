﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using NavicHelper.UI.Converters;
using NavicHelper.UI.Extensions;
using NavicHelper.UI.Managers;
using NaviсHelper.Lib.Models;
using NaviсHelper.Lib.ViewModels;
using NaviсHelper.Lib.ViewModels.Profile;
using Prism.Commands;
using Prism.Mvvm;

namespace NavicHelper.UI
{
    internal class MainViewModel: BindableBase
    {
        private DeviceTypeViewModel DeviceTypeViewModel { get; }
        private PortViewModel PortViewModel { get; }
        private ProfilesViewModel ProfileViewModel { get; }
        private FlashingViewModel FlashingViewModel { get; }

        private readonly ProfileManager m_profileManager = new ProfileManager();
        private readonly FlasherManager m_flasherManager = new FlasherManager();
        private readonly FirmwareManager m_firmwareManager = new FirmwareManager();
        private readonly SettingsManager m_settingsManager = new SettingsManager();

        public MainViewModel()
        {
            m_settingsManager.ReadConfig();

            m_profileManager.OnProfileChanged += profile => m_flasherManager.Profile = profile;

            m_profileManager.SetConfiguration(m_settingsManager.Configuration);
            m_flasherManager.SetSettings(m_settingsManager.Configuration);

            DeviceTypeViewModel = m_flasherManager.DeviceTypeViewModel;
            DeviceTypeViewModel.PropertyChanged += SetConfigProperty;

            PortViewModel = m_flasherManager.PortViewModel;
            PortViewModel.PropertyChanged += SetConfigProperty;

            FlashingViewModel = m_flasherManager.FlashingViewModel;
            FlashingViewModel.PropertyChanged += (sender, args) => RaisePropertyChanged(args.PropertyName);

            ProfileViewModel = m_profileManager.ProfileViewModel;
            ProfileViewModel.PropertyChanged += (sender, args) => RaisePropertyChanged(args.PropertyName);
        }

        #region Properties

        /// <summary>
        /// Ком-порты системы
        /// </summary>
        public ReadOnlyObservableCollection<string> ComPorts => PortViewModel.ComPorts;

        /// <summary>
        /// Данные получение с ком-порта
        /// </summary>
        public string ComPortData => FlashingViewModel.ComPortData;

        /// <summary>
        /// Выбранный тип устройства
        /// </summary>
        public NavicType NavicType
        {
            get => DeviceTypeViewModel.NavicType;
            set
            {
                m_profileManager.ChangeProfile(value);
                DeviceTypeViewModel.NavicType = value;
            }
        }

        /// <summary>
        /// Состояние работы приложения
        /// </summary>
        public Status Status => PortViewModel.Status;

        /// <summary>
        /// Изменились ли настройки после последней успешной прошивки
        /// </summary>
        public bool IsConfigChanged => true;// SelectedProfile.IsConfigChange;

        public ProfileViewModel SelectedProfile => ProfileViewModel.SelectedProfile;

        #endregion Properties

        #region Commands

        /// <summary>
        /// Сканирование ком-портов системы
        /// </summary>
        public ICommand ScanPortsCommand => new DelegateCommand(m_flasherManager.RescanComPorts, () => Status == Status.STATUS_OFF);

        /// <summary>
        /// Прикрепление файла прошивки к объекту
        /// </summary>
        public ICommand AddFirmwareCommand => new DelegateCommand<FirmwareModel>(m_firmwareManager.ChoseFirmware);

        /// <summary>
        /// Удаления файла прошивки из объекта
        /// </summary>
        public ICommand RemoveFirmwareCommand => new DelegateCommand<FirmwareModel>(m_firmwareManager.RemoveFirmware);

        /// <summary>
        /// Открытие порта и начало прослушки
        /// </summary>
        public ICommand ConnectCommand => new DelegateCommand(m_flasherManager.Connect);

        /// <summary>
        /// Отмена текущего процесса прошивки
        /// </summary>
        public ICommand CancelCommand => new DelegateCommand(m_flasherManager.Cancel);

        /// <summary>
        /// Закрытие порта
        /// </summary>
        public ICommand DisconnectCommand => new DelegateCommand(m_flasherManager.Stop);

        /// <summary>
        /// Очистка текст-бокса с полученными данными от порта
        /// </summary>
        public ICommand CleanPortDataCommand => new DelegateCommand(m_flasherManager.CleanLog);

        /// <summary>
        /// Проверка моделей и запуск процесса прошивки
        /// </summary>
        public ICommand StartCommand => new DelegateCommand(() =>
        {
            try
            {
                m_flasherManager.Start();
            }
            catch (Exception exception)
            {
                AppExtensions.ShowError(exception.Message);
            }
        });

        /// <summary>
        /// Сохранение параметров прошивки в случае изменения их и успешного завершения процесса прошивки
        /// </summary>
        public ICommand SaveConfigCommand => new DelegateCommand(() =>
        {
            var converter = new ProfileConverter();
            m_settingsManager.SaveConfig(new Configuration
            {
               BaudRate = PortViewModel.BaudRate,
               NavicType = NavicType,
               Profiles = ProfileViewModel.Profiles.Select(model => converter.ToModel(model)).ToArray()
            });
        });

        /// <summary>
        /// Процесс закончен, очистка библиотеки, закрытие диалогового окна
        /// </summary>
        public ICommand FlashingDoneCommand => new DelegateCommand(m_flasherManager.Reset);

        /// <summary>
        /// Выход из приложения
        /// </summary>
        public ICommand CloseApplicationCommand => new DelegateCommand<object>(obj =>
        {
            if (obj is Window window) window.Close();
        });

        /// <summary>
        /// Сворачивание приложения
        /// </summary>
        public ICommand MinimizeCommand => new DelegateCommand<object>(obj =>
        {
            if (obj is Window window) window.WindowState = WindowState.Minimized;
        });

        public ICommand UpdateCommand => new DelegateCommand(() =>
        {
            m_flasherManager.Update((SelectedProfile as NavicViewModel)?.MotherboardFirmware.Path);
        });

        public ICommand BootCommand => new DelegateCommand(() =>
        {
            m_flasherManager.Boot((SelectedProfile as NavicViewModel)?.MotherboardFirmware.Path);
        });

        #endregion Commands

        #region Methods

        private void SetConfigProperty(object model, PropertyChangedEventArgs args)
        {
            var modelValue = model.GetType().GetProperty(args.PropertyName)?.GetValue(model);
            var property = m_settingsManager.Configuration.GetType().GetProperty(args.PropertyName);

            if (property != null)
            {
                //  property.SetValue(m_settingsManager.Configuration, modelValue);
                RaisePropertyChanged(nameof(IsConfigChanged));
            }

            RaisePropertyChanged(args.PropertyName);
        }

        #endregion Methods
    }
}