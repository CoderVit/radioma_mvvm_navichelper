﻿using System;
using System.Collections.Generic;
using System.Windows;

namespace NavicHelper.UI.Extensions
{
    internal static class AppExtensions
    {
        public static void ShowError(string error) => MessageBox.Show(error, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);

        public static void ShowError(List<string> errors) => ShowError(string.Join(Environment.NewLine, errors));

        public static void ShowError(Exception exception) => ShowError(exception.ToString());
    }
}