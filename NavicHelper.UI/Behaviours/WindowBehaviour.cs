﻿using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace NavicHelper.UI.Behaviours
{
    internal class WindowBehaviour
    {
        public static readonly DependencyProperty EnableDragProperty = DependencyProperty.RegisterAttached("EnableDrag", typeof(bool), typeof(WindowBehaviour), new PropertyMetadata(default(bool), OnLoaded));

        private static void OnLoaded(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            if (!(dependencyObject is UIElement uiElement) || (dependencyPropertyChangedEventArgs.NewValue is bool) == false)
            {
                return;
            }

            if ((bool)dependencyPropertyChangedEventArgs.NewValue)
            {
                uiElement.MouseMove += WindowOnMouseMove;
            }
            else
            {
                uiElement.MouseMove -= WindowOnMouseMove;
            }

        }

        private static void WindowOnMouseMove(object sender, MouseEventArgs mouseEventArgs)
        {
            if (!(sender is UIElement uiElement) || mouseEventArgs.LeftButton != MouseButtonState.Pressed) return;

            DependencyObject parent = uiElement;
            var avoidInfiniteLoop = 0;
            // Search up the visual tree to find the first parent window.
            while ((parent is Window) == false)
            {
                parent = VisualTreeHelper.GetParent(parent);
                avoidInfiniteLoop++;
                if (avoidInfiniteLoop == 1000) return;
            }
            var window = parent as Window;
            window.DragMove();
        }

        public static bool GetEnableDrag(DependencyObject element)
        {
            return (bool)element.GetValue(EnableDragProperty);
        }

        public static void SetEnableDrag(DependencyObject element, bool value)
        {
            element.SetValue(EnableDragProperty, value);
        }
    }
}