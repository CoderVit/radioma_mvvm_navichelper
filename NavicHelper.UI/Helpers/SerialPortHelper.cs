﻿using System.IO.Ports;

namespace NavicHelper.UI.Helpers
{
    internal class SerialPortHelper
    {
        public string[] GetComPorts => SerialPort.GetPortNames();
    }
}