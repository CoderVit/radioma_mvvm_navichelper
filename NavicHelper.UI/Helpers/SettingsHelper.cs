﻿using System;
using NavicHelper.UI.Properties;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using NaviсHelper.Lib.Models;
using NaviсHelper.Lib.Models.Serializations;

namespace NavicHelper.UI.Helpers
{
    internal class SettingsHelper
    {
        private readonly XmlSerializer m_xmlSerializer = new XmlSerializer(typeof(Configuration), new[] { typeof(Navic), typeof(NavicPlus) });
        private readonly int[] m_baudRates = { 9600, 115200 };

        /// <summary>
        /// Адрес приложения в разделе AppData
        /// </summary>
        private string AppDataPath { get; }

        /// <summary>
        /// Путь файла конфигурации
        /// </summary>
        private string SettingsPath { get; }

        public SettingsHelper()
        {
            var folderPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            var folderName = Path.GetFileNameWithoutExtension(AppDomain.CurrentDomain.FriendlyName);
            AppDataPath = Path.Combine(folderPath, folderName);

            if (!Directory.Exists(AppDataPath)) Directory.CreateDirectory(AppDataPath);

            SettingsPath = Path.Combine(AppDataPath, "config.gh");
        }

        public Configuration ReadConfig()
        {
            var settings = Settings.Default;

            var config = new Configuration
            {
                BaudRate = settings != null && m_baudRates.Any(rate => rate == settings.Speed) ? settings.Speed : m_baudRates.Last()
            };

            if (File.Exists(SettingsPath))
            {
                try
                {
                    using (var fileStream = File.Open(SettingsPath, FileMode.Open, FileAccess.Read, FileShare.Read))
                    {
                        var conf = (Configuration)m_xmlSerializer.Deserialize(fileStream);
                        config.Profiles = conf.Profiles;
                        config.NavicType = conf.NavicType;
                    }
                }
                catch { }
            }

            return config;
        }

        public void SaveConfiguration(Configuration configuration)
        {
            using (var stream = new StreamWriter(SettingsPath))
            {
                m_xmlSerializer.Serialize(stream, configuration);
            }
        }
    }
}