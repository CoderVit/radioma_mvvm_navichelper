﻿using System.Windows;
using NaviсHelper.Lib.Models;

namespace NavicHelper.UI.Views
{
    public partial class FirmwareSelectorView
    {
        public FirmwareSelectorView()
        {
            InitializeComponent();
        }

        private static readonly DependencyProperty m_controlDescription = DependencyProperty.Register(
            nameof(ControlDescription),
            typeof(string),
            typeof(FirmwareSelectorView));

        public string ControlDescription
        {
            get => (string)GetValue(m_controlDescription);
            set => SetValue(m_controlDescription, value);
        }

        private static readonly DependencyProperty m_firmware = DependencyProperty.Register(
            nameof(Firmware),
            typeof(FirmwareModel),
            typeof(FirmwareSelectorView));

        public FirmwareModel Firmware
        {
            get => (FirmwareModel)GetValue(m_firmware);
            set => SetValue(m_firmware, value);
        }

        private static readonly DependencyProperty m_firmwareProgress = DependencyProperty.Register(
            nameof(FirmwareProgress),
            typeof(int),
            typeof(FirmwareSelectorView));

        public int FirmwareProgress
        {
            get => (int)GetValue(m_firmwareProgress);
            set => SetValue(m_firmwareProgress, value);
        }
    }
}