﻿using NaviсHelper.Lib.Models.Serializations;
using NaviсHelper.Lib.ViewModels.Profile;

namespace NavicHelper.UI.Converters
{
    internal class ProfileConverter
    {
        public ProfileViewModel ToViewModel(Profile profile)
        {
            switch (profile)
            {
                case Navic navicProfile:
                    return new NavicViewModel
                    {
                        ComPort = navicProfile.ComPort,
                        MotherboardFirmware = { Path = navicProfile.MotherboardFirmwarePath },
                        ModulesFirmware = { Path = navicProfile.ModulesFirmwarePath }
                    };
                case NavicPlus navicProfilePlus:
                    return new NavicPlusViewModel
                    {
                        ComPort = navicProfilePlus.ComPort,
                        Firmware = { Path = navicProfilePlus.FirmwarePath }
                    };
                default:
                    return null;
            }
        }

        public Profile ToModel(ProfileViewModel profileModel)
        {
            switch (profileModel)
            {
                case NavicViewModel profileViewModel:
                    return new Navic
                    {
                        ComPort = profileViewModel.ComPort,
                        ModulesFirmwarePath = profileViewModel.ModulesFirmware.Path,
                        MotherboardFirmwarePath = profileViewModel.MotherboardFirmware.Path
                    };
                case NavicPlusViewModel profilePlusViewModel:
                    return new NavicPlus
                    {
                        ComPort = profilePlusViewModel.ComPort,
                        FirmwarePath = profilePlusViewModel.Firmware.Path
                    };
                default:
                    return null;
            }
        }
    }
}