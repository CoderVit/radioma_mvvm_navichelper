﻿using System;
using System.Globalization;
using System.Windows.Data;
using NaviсHelper.Lib.Models;

namespace NavicHelper.UI.Converters
{
    /// <summary>
    /// Иерархический конвертер статуса процесса прошивки устройств.
    /// Проверка, выше ли текущий статус процесса с заданным.
    /// </summary>
    internal class StatusConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null || !Enum.TryParse(value.ToString(), out Status status)) return false;
            if (parameter == null || !Enum.TryParse(parameter.ToString(), out Status parameterStatus)) return false;

            return parameterStatus <= status;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}