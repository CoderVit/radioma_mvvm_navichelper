﻿using System;
using NavicHelper.UI.Converters;
using System.Linq;
using NaviсHelper.Lib.Models;
using NaviсHelper.Lib.Models.Serializations;
using NaviсHelper.Lib.ViewModels;
using NaviсHelper.Lib.ViewModels.Profile;

namespace NavicHelper.UI.Managers
{
    internal class ProfileManager
    {
        private readonly ProfileConverter m_profileConverter = new ProfileConverter();

        public ProfilesViewModel ProfileViewModel { get; } = new ProfilesViewModel();

        #region Delegates

        /// <summary>
        /// Активация, при смене профиля устройства
        /// </summary>
        public Action<ProfileViewModel> OnProfileChanged;

        #endregion Delegates

        #region Methods

        public void SetConfiguration(Configuration configuration)
        {
            (configuration.Profiles ?? new Profile[] { new Navic(), new NavicPlus() }).ToList().ForEach(profile => ProfileViewModel.AddProfile(m_profileConverter.ToViewModel(profile)));
            ChangeProfile(configuration.NavicType);
        }

        public void ChangeProfile(NavicType navicType)
        {
            ProfileViewModel.SelectedProfile = ProfileViewModel.Profiles.FirstOrDefault(model => model.NavicType == navicType);
            OnProfileChanged?.Invoke(ProfileViewModel.SelectedProfile);
        }

        #endregion Methods
    }
}