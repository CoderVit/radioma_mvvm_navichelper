﻿using Microsoft.Win32;
using NaviсHelper.Lib.Models;

namespace NavicHelper.UI.Managers
{
    internal class FirmwareManager
    {
        public void ChoseFirmware(FirmwareModel firmware)
        {
            if (firmware == null) return;

            var fileDialog = new OpenFileDialog
            {
                DefaultExt = ".bin",
                Filter = "Файлы прошивок (*.bin)|*.bin",
                Title = "Выберите файлы прошивок"
            };

            if (fileDialog.ShowDialog() != true) return;

            firmware.Path = fileDialog.FileName;
        }

        public void RemoveFirmware(FirmwareModel firmware)
        {
            if (firmware != null)
            {
                firmware.Path = null;
            }
        }
    }
}