﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using NavicHelper.UI.Extensions;
using NavicHelper.UI.Helpers;
using NaviсHelper.Lib.Flashers;
using NaviсHelper.Lib.Models;
using NaviсHelper.Lib.Protocols;
using NaviсHelper.Lib.Steps.NavicProPlus;
using NaviсHelper.Lib.ViewModels;
using NaviсHelper.Lib.ViewModels.Profile;

namespace NavicHelper.UI.Managers
{
    internal class FlasherManager
    {
        private readonly SerialPortHelper m_portHelper = new SerialPortHelper();

        public DeviceTypeViewModel DeviceTypeViewModel { get; } = new DeviceTypeViewModel();
        public FlashingViewModel FlashingViewModel { get; } = new FlashingViewModel();
        public PortViewModel PortViewModel { get; } = new PortViewModel();

        /// <summary>
        /// Выбранный профиль
        /// </summary>
        public ProfileViewModel Profile { get; set; }

        private SerialPort m_serialPort;

        private NavicFlasher m_flasher;
        private NavicPlusFlasher m_flasherPlus;

        public FlasherManager() => RescanComPorts();

        /// <summary>
        /// Установка настроек из сохранённого файла конфигурации
        /// </summary>
        /// <param name="configuration">файл конфигурации</param>
        public void SetSettings(Configuration configuration)
        {
            PortViewModel.BaudRate = configuration.BaudRate;
            DeviceTypeViewModel.NavicType = configuration.NavicType;
        }

        /// <summary>
        /// Создание класса, что будет прошивать устройство.
        /// В зависимости от выбранного типа оборудования.
        /// </summary>
        public void Connect()
        {
            if (PortViewModel.Status != Status.STATUS_OFF) return;

            var errors = new List<string>();

            if (string.IsNullOrEmpty(Profile.ComPort))
            {
                errors.Add("Не задан КОМ-порт");
            }
            PortViewModel.Validate(errors);

            if (errors.Count == 0)
            {
                m_serialPort = new SerialPort(Profile.ComPort, PortViewModel.BaudRate, Parity.None, 8, StopBits.One);

                try
                {
                    m_serialPort.Open();
                    m_serialPort.DataReceived += OnSerialPort_DataReceived;
                    PortViewModel.Status = Status.STATUS_CONNECTED;
                }
                catch (Exception exception)
                {
                    AppExtensions.ShowError(exception.Message);
                }
            }
            else AppExtensions.ShowError(errors);
        }

        /// <summary>
        /// Прерывание текущего процесса прошивки.
        /// </summary>
        public void Cancel()
        {
            if (m_flasher != null)
            {
                m_flasher.Cancel();
                m_flasher = null;
            }

            if (m_flasherPlus != null)
            {
                m_flasherPlus.Cancel();
                m_flasherPlus = null;
            }

            PortViewModel.Status = Status.STATUS_CONNECTED;
        }

        /// <summary>
        /// Запуск процесса прошивки.
        /// Переход приложения в статус "Flashing"
        /// </summary>
        public void Start()
        {
            var errors = new List<string>();

            if (PortViewModel.Status != Status.STATUS_CONNECTED)
            {
                errors.Add("Устройство не подключено");
            }

            Profile.Validate(errors);

            if (!errors.Any())
            {
                if (Profile is NavicViewModel navicProfile)
                {
                    m_flasher = new NavicFlasher(m_serialPort);
                    m_flasher.OnFlashingModules += percent => navicProfile.ModulesFlashingProcess = percent;
                    m_flasher.OnFlashingMotherBoard += percent => navicProfile.MotherboardFlashingProcess = percent;
                    m_flasher.OnDataReceived += data => FlashingViewModel.ComPortData += data;
                    m_flasher.OnErrorOccured += data => FlashingViewModel.ComPortData += data;
                    m_flasher.OnProcessFinish += () => PortViewModel.Status = Status.STATUS_DONE;

                    m_flasher.InitFirmware(navicProfile.ModulesFirmware.Path, navicProfile.MotherboardFirmware.Path); //отправка команды на начало перепрошивки
                    m_flasher.Start();
                }
                else if (Profile is NavicPlusViewModel navicPlusProfile)
                {
                    var xModemClient = new Client(new XModem(m_serialPort));

                    m_flasherPlus = new NavicPlusFlasher(m_serialPort, xModemClient);
                    m_flasherPlus.FlashingModules += percent => navicPlusProfile.FirmwareProgress = percent;
                    m_flasherPlus.DataReceived += data => FlashingViewModel.ComPortData += data;
                    m_flasherPlus.ErrorOccured += data => FlashingViewModel.ComPortData += data + Environment.NewLine;
                    m_flasherPlus.OnProcessFinish += () => PortViewModel.Status = Status.STATUS_DONE;

                    xModemClient.SendProgress += m_flasherPlus.OnFlashingModules;

                    var step = new NavicPlusHandler(m_serialPort, navicPlusProfile.Firmware.Path);
                    m_flasherPlus.InitHandler(step);
                    m_flasherPlus.Start();
                }

                //m_serialPort.DataReceived -= OnSerialPort_DataReceived;
                PortViewModel.Status = Status.STATUS_FLASHING;
            }
            else
            {
                AppExtensions.ShowError(errors);
            }
        }

        /// <summary>
        /// Переход приложения в статус "OFF".
        /// </summary>
        public void Stop()
        {
            if (PortViewModel.Status < Status.STATUS_CONNECTED) return;

            try
            {
                m_serialPort.Close();
                m_flasher = null;
                m_flasherPlus = null;
                PortViewModel.Status = Status.STATUS_OFF;
            }
            catch (Exception exception)
            {
                AppExtensions.ShowError(exception.Message);
            }
        }

        public void Update(string motherboardFirmware) => m_flasher?.RunFromUpdater(motherboardFirmware);

        public void Boot(string motherboardFirmware) => m_flasher?.RunFromBoot(motherboardFirmware);

        public void Reset()
        {
            if (PortViewModel.Status != Status.STATUS_DONE) throw new Exception("Произошла ошибка");
            Profile.ResetProgress();
            Stop();
            PortViewModel.Status = Status.STATUS_OFF;   //в Stop() уже есть
        }

        public void RescanComPorts() => PortViewModel.UpdateComPorts(m_portHelper.GetComPorts);

        public void CleanLog() => FlashingViewModel.ComPortData = string.Empty;

        private void OnSerialPort_DataReceived(object sender, SerialDataReceivedEventArgs args)
        {
            var response = m_serialPort.ReadExisting();

            m_flasher?.Handle(response);
            m_flasherPlus?.Handle(response);

            FlashingViewModel.ComPortData += response;
        }
    }
}