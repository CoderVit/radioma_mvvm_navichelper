﻿using NavicHelper.UI.Extensions;
using NavicHelper.UI.Helpers;
using NaviсHelper.Lib.Models;

namespace NavicHelper.UI.Managers
{
    internal class SettingsManager
    {
        private readonly SettingsHelper m_settingsHelper = new SettingsHelper();

        private Configuration SavedConfig { get; set; }

        public Configuration Configuration { get; set; }

        public bool IsConfigChanged => !SavedConfig.Equals(Configuration);

        #region Methods

        /// <summary>
        /// Чтение файла конфигурации
        /// </summary>
        public void ReadConfig()
        {
            try
            {
                Configuration = m_settingsHelper.ReadConfig();
                SavedConfig = (Configuration)Configuration.Clone();
            }
            catch
            {
                AppExtensions.ShowError("Ошибка чтения файла настроек");
            }
        }

        /// <summary>
        /// Сохранение файла конфигурации
        /// </summary>
        public void SaveConfig(Configuration configuration)
        {
            if (!IsConfigValid) return;

            try
            {
                m_settingsHelper.SaveConfiguration(configuration);
                SavedConfig = (Configuration)Configuration.Clone();
            }
            catch
            {
                AppExtensions.ShowError("Ошибка сохранения");
            }
        }

        public bool IsConfigValid => /*!string.IsNullOrEmpty(Configuration.ComPort)
                                     && */Configuration.BaudRate != 0;
                                    /*&& !string.IsNullOrEmpty(Configuration.ModulesFirmware)
                                    && !string.IsNullOrEmpty(Configuration.MotherboardFirmware);*/

        #endregion Methods
    }
}